;;;;;;;;;;;;;
;;MODE GAME;;
;;;;;;;;;;;;;
(fn update-game [])

(fn draw-game []
 (cls 0)
 (print-center "game" 120 60 10 2))

(fn input-game [])

(fn mode-game []
 (tset *mode* :update update-game)
 (tset *mode* :draw draw-game)
 (tset *mode* :input input-game))
