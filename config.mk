# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (c) 2021 parlortricks

# Tic80 globals pulled from the wiki https://github.com/nesbox/TIC-80/wiki/API
TIC_GLOBALS=TIC,SCN,OVR,btn,btnp,clip,cls,circ,circb,elli,ellib,exit,fget,fset,font,key,keyp,line,map,memcpy,memset,mget,mset,mouse,music,peek,peek4,pix,pmem,poke,poke4,print,rect,rectb,reset,sfx,spr,sync,time,tstamp,trace,tri,trib,textri

# Name of the empty .tic cart you have created
CART=ducklike

# itch.io username
ITCH_USER=parlortricks

# itch.io project name
PROJECT=ducklike

# Store the current date/time
CURDATE=`date +'%y.%m.%d-%H:%M:%S'`

# Where do we store builds
BUILD_DIR=./build

# What's the source file directory
SRC_DIR=./src/

# List of files and what order to use them in
SRC=$(SRC_DIR)header.fnl \
$(SRC_DIR)utils.fnl \
$(SRC_DIR)mode-over.fnl \
$(SRC_DIR)mode-game.fnl \
$(SRC_DIR)mode-menu.fnl \
$(SRC_DIR)mode-intro.fnl \
$(SRC_DIR)game.fnl

