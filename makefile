# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (c) 2021 parlortricks
include config.mk

compile: out.fnl
	@PATH=$(PATH):/home/allan/bin; ls $(SRC_DIR)*fnl | entr make out.fnl & echo "$$!" > "compile.pid"

run: out.fnl
	@PATH=$(PATH):/home/allan/bin; tic80 --skip --fs . --cmd "load $(CART).tic & import code out.fnl & run"

out.fnl: $(SRC) ; cat $^ > $@

check: out.fnl ; fennel --globals $(TIC_GLOBALS) --compile $< > /dev/null

compile-lua: out.fnl ; fennel --globals $(TIC_GLOBALS) --compile $< > out.lua

format: out.fnl
	fnlfmt --fix out.fnl

count: out.fnl ; cloc $^

export-zip: out.fnl
	mkdir -p build
	tic80 --cli --fs $(CURDIR) --cmd "load $(CART).tic & import code out.fnl & export html $(CART) & exit"
	mv $(CURDIR)/$(CART).zip $(CURDIR)/build/$(CART).zip

export-cart: out.fnl
	mkdir -p build
	tic80 --cli --fs . --cmd "load $(CART).tic & import code out.fnl & save out.tic & exit"
	mv $(CURDIR)/out.tic $(CURDIR)/build/$(CART).tic

export-png: out.fnl
	mkdir -p build
	tic80 --cli --fs . --cmd "load $(CART).tic & import code out.fnl & save out.png & exit"
	mv $(CURDIR)/out.png $(CURDIR)/build/$(CART).png

export-win: out.fnl
	mkdir -p build
	tic80 --cli --fs . --cmd "load $(CART).tic & import code out.fnl & export win out.exe alone=1 & exit"
	mv $(CURDIR)/out.exe $(CURDIR)/build/$(CART).exe

export-linux: out.fnl
	mkdir -p build
	tic80 --cli --fs . --cmd "load $(CART).tic & import code out.fnl & export linux out alone=1 & exit"
	mv $(CURDIR)/out $(CURDIR)/build/$(CART)

export-mac: out.fnl
	mkdir -p build
	tic80 --cli --fs . --cmd "load $(CART).tic & import code out.fnl & export mac out.app alone=1 & exit"
	mv $(CURDIR)/out.app $(CURDIR)/build/$(CART).app	

upload: $(CURDIR)/build/$(CART).zip
	butler push $(CURDIR)/build/$(CART).zip $(ITCH_USER)/$(PROJECT):$(CART)

status: 
	butler status $(ITCH_USER)/$(PROJECT):$(CART)

todo: ; grep -in TODO $(SRC)
fixme: ; grep -in FIXME $(SRC)

clean: 
	rm -rf build
	rm -rf out.fnl
	rm -rf .local
	cat compile.pid | xargs kill -9
	rm -rf compile.pid
