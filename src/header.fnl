;; title:   ducklike
;; author:  parlortricks
;; desc:    a rogue-like game
;; website: https://parlortricks.itch.io/build-template
;; script:  fennel
;; saveid:  parlortricks_ducklike_1
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Copyright (c) 2022 parlortricks

(local screen-width 240)
(local screen-height 136)
(local sin math.sin)
(local cos math.cos)
(local atan2 math.atan2)
(local pi math.pi)
(local pause false)
(local palette-map 0x3ff0)

;;;;;;;;;;;;;
;;VARIABLES;;
;;;;;;;;;;;;;
(var t 0)
(var *mode* {})
(var *player* {})
(var intro-text-left -8)
(var intro-text-right 240)
(var intro-timer 0)
(var t 0)
(var menu-pos 0)
