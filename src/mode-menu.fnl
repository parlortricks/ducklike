;;;;;;;;;;;;;
;;MODE MENU;;
;;;;;;;;;;;;;
(fn update-menu [])

(fn draw-menu []  
 (var color 5)
 (var menu-pos-y 79)
 (cls 13)
 (for [x 0 29]
  (for [y 0 16]
    (if (= (% t 2) 0)
     (set color 5)
     (set color 6))
    (rect (* x 8) (* y 8) 8 8 color)
    (set t (+ t 1))))
 (spr-shadow 112 60 15 5 -2 2 15 4)
 (spr-shadow 180 100 40 5 -2 2 11 4)
 
 (if (= menu-pos 0)
      (set menu-pos-y 79))

  (if (= menu-pos 1)
      (set menu-pos-y 90)
     (= menu-pos 2)
      (set menu-pos-y 101)
     (= menu-pos 3)
      (set menu-pos-y 112))
      
 (spr 7 75 menu-pos-y 5)
 (spr 7 155 menu-pos-y 5)

 (print-center-outline "start game" 120 80 3 0 1)
 (print-center-outline "options" 120 91 3 0 1)
 (print-center-outline "high score" 120 102 3 0 1)
 (print-center-outline "credits" 120 113 3 0 1)



 )

(fn input-menu []
 (when (btnp 0)
  (set menu-pos (- menu-pos 1)))
 (when (btnp 1)
  (set menu-pos (+ menu-pos 1)))

 (if (< menu-pos 0)
  (set menu-pos 3)
     (> menu-pos 3)
  (set menu-pos 0))

 (when (and (or (btnp 4) (btnp 5)) (= menu-pos 0))
  (mode-game)))

(fn mode-menu []
 (tset *mode* :update update-menu)
 (tset *mode* :draw draw-menu)
 (tset *mode* :input input-menu))
