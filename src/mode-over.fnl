;;;;;;;;;;;;;
;;MODE OVER;;
;;;;;;;;;;;;;
(fn update-over [])
(fn draw-over [])
(fn input-over [])

(fn mode-over []
 (tset *mode* :update update-over)
 (tset *mode* :draw draw-over)
 (tset *mode* :input input-over))
