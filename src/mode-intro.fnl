;;;;;;;;;;;;;;
;;MODE INTRO;;
;;;;;;;;;;;;;;
(fn update-intro []
 (if (< intro-text-left 57)
  (set intro-text-left (+ intro-text-left 1))
  (set intro-text-left 57))

 (if (> intro-text-right 125)
  (set intro-text-right (- intro-text-right 1.75))
  (set intro-text-right 125))
  
 (if (> intro-timer 180)
  (mode-menu)
  (set intro-timer (+ intro-timer 1))))

(fn draw-intro []
 (cls 0)
 (print "a" intro-text-left 54)
 (print-center "kobalt haus" 120 60 10 2)
 (print "production" intro-text-right 71))

(fn input-intro []
 (when (or (btnp 4) (btnp 5))
  (mode-menu)))

(fn mode-intro []
 (tset *mode* :update update-intro)
 (tset *mode* :draw draw-intro)
 (tset *mode* :input input-intro))
