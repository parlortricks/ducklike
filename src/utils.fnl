;;;;;;;;;
;;UTILS;;
;;;;;;;;;
(fn print-center [str x y color scale]
 (let [pixel-width (* (print str -50 -50 -8) scale)]
  (print str (- x (/ pixel-width 2)) y color false scale)))

(fn spr-shadow [sp sx sy tcol dx dy sw sh]
 (for [c 0 15]
  (poke4 (+ (* palette-map 2) c) 0))
 (spr sp (+ sx dx) (+ sy dy) tcol 1 0 0 sw sh)
 (for [c 0 15]
  (poke4 (+ (* palette-map 2) c) c))
 (spr sp sx sy tcol 1 0 0 sw sh))

(fn print-outline [str x y color color2]
 (for [dx -1 1]
  (for [dy -1 1]
  (print str (+ x dx) (+ y dy) color2)))
 (print str x y color))

(fn print-center-outline [str x y color color2 scale]
 (let [pixel-width (* (print str -50 -50 -8) scale)]
  (for [dx -1 1]
   (for [dy -1 1]
    (print str (+ (- x (/ pixel-width 2)) dx) (+ y dy) color2 false scale)))
  (print str (- x (/ pixel-width 2)) y color false scale)))
